import { render, screen } from '@testing-library/react';

import { composeStories } from '@storybook/react';

import * as stories from './Toast.stories';

const { Simmple } = composeStories(stories);

test('reuses args from composed story', () => {
  render(<Simmple />);
  
  const children = screen.getByText('The action that you have done was a success! Well done');
  // Testing against values coming from the story itself! No need for duplication
  expect(children.textContent).toEqual(Simmple.args.children);
});