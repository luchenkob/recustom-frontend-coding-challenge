import React from 'react';
import {
  Snackbar,
  Alert,
  Typography,
  IconButton,
  Box,
  AlertTitle,
  Button,
} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import { alpha } from '@mui/material';

export interface ToastProps {
  open?: boolean;
  severity?: 'success' | 'info' | 'warning' | 'error';
  variant?: 'filled' | 'standard' | 'outlined';
  disableTitleShape?: boolean;
  children?: React.ReactNode;
  icon?: React.ReactNode;
  title?: React.ReactNode;
  button?: React.ReactNode;
  enableOffset?: boolean;
  onClose?: () => void;
  onButtonClick?: () => void;
}

const Toast = ({
  open = true,
  severity = 'success',
  variant = 'outlined',
  enableOffset,
  disableTitleShape,
  onClose,
  children,
  icon,
  title,
  button,
  onButtonClick,
  ...rest
}: ToastProps) => {
  return (
    <Snackbar
      {...rest}
      open={open}
      onClose={onClose}
      sx={{ maxWidth: '640px' }}
    >
      <Alert
        {...(icon
          ? {
              icon: disableTitleShape ? (
                <Box sx={{ marginTop: '0.25rem' }}>{icon}</Box>
              ) : (
                <Box
                  width={`2rem`}
                  height={`2rem`}
                  borderRadius={`7px`}
                  display={`flex`}
                  alignItems={`center`}
                  justifyContent={`center`}
                  sx={{
                    backgroundColor: (theme) =>
                      alpha(theme.palette[severity].main, 0.1),
                  }}
                >
                  {icon}
                </Box>
              ),
            }
          : {})}
        onClose={onClose}
        severity={severity}
        variant={variant}
        sx={{ width: '100%', '&& > *': { overflow: 'visible' } }}
        action={
          <IconButton
            sx={{ marginTop: `2px` }}
            aria-label="close"
            size="small"
            onClick={onClose}
          >
            <CloseIcon
              sx={{
                color: (theme) =>
                  variant === 'filled' ? 'white' : theme.palette[severity].main,
              }}
              fontSize="inherit"
            />
          </IconButton>
        }
      >
        {title && (
          <AlertTitle
            sx={{
              fontWeight: '600',
              fontSize: '0.875rem',
              marginTop: '0.25rem',
            }}
            color={`${variant === 'filled' ? `white` : `${severity}.main`}`}
          >
            {title}
          </AlertTitle>
        )}
        <Box
          sx={{
            marginLeft:
              title && !enableOffset
                ? disableTitleShape
                  ? '-2rem'
                  : '-2.5rem'
                : 0,
            marginTop: title ? '0.5rem' : 0,
            position: 'relative',
            zIndex: '2',
          }}
        >
          <Typography
            color={`${variant === 'filled' ? `white` : `${severity}.main`}`}
            variant="body1"
            sx={{
              fontSize: '0.875rem',
            }}
          >
            {children}
          </Typography>
          {button && (
            <Button
              onClick={() => {
                if (onButtonClick) onButtonClick();
              }}
              sx={{ marginTop: '0.75rem' }}
              color={severity}
              variant="contained"
              size="small"
            >
              {button}
            </Button>
          )}
        </Box>
      </Alert>
    </Snackbar>
  );
};

export default Toast;
