import type { Meta, StoryObj } from '@storybook/react';
import { fn } from '@storybook/test';
import Toast, { ToastProps } from '.';
import { useArgs } from '@storybook/preview-api';
import DoneIcon from '@mui/icons-material/Done';
import NotificationsNoneIcon from '@mui/icons-material/NotificationsNone';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import { Box } from '@mui/material';

const meta: Meta<typeof Toast> = {
  title: 'Example/Toast',
  component: Toast,
  parameters: {
    layout: 'centered',
  },
  tags: ['autodocs'],
  argTypes: {
    open: { control: 'boolean' },
    enableOffset: { control: 'boolean' },
    disableTitleShape: { control: 'boolean' },
    title: { control: 'text' },
    button: { control: 'text' },
  },
  args: { onClose: fn(), onButtonClick: fn() },
};

export default meta;

type Story = StoryObj<typeof Toast>;

const renderTemplate = (args: ToastProps) => {
  const [{ open }, updateArgs] = useArgs();

  const onClose = () => {
    updateArgs({ open: !open });
  };

  return <Toast {...args} onClose={onClose} open={open} />;
};

export const Simmple: Story = {
  args: {
    open: true,
    icon: <DoneIcon />,
    variant: 'outlined',
    children: 'The action that you have done was a success! Well done',
  },
  render: renderTemplate,
};

export const Secondary: Story = {
  args: {
    open: true,
    icon: <NotificationsNoneIcon />,
    variant: 'outlined',
    severity: 'error',
    children: (
      <span>
        The file <strong>flowbite-figma-pro</strong> was permanently deleted.
      </span>
    ),
  },
  render: renderTemplate,
};

export const Success: Story = {
  args: {
    open: true,
    icon: <CheckCircleIcon />,
    disableTitleShape: true,
    variant: 'outlined',
    title: 'Success',
    severity: 'success',
    button: 'Take action',
    children:
      'Well done, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content. Be sure to use margin utilities to keep things nice and tidy.',
  },
  render: renderTemplate,
};

export const UserNotification: Story = {
  args: {
    open: true,
    icon: (
      <Box
        component="img"
        sx={{
          height: '2rem',
          width: '2rem',
          maxHeight: { xs: '2rem', md: '2rem' },
          maxWidth: { xs: '2rem', md: '2rem' },
        }}
        alt="Avatar"
        src="/avatar.png"
      />
    ),
    disableTitleShape: true,
    enableOffset: true,
    variant: 'outlined',
    title: 'Bonnie Green',
    severity: 'info',
    button: 'Button text',
    children: 'Hi Neil, thanks for sharing your thoughts regardingFlowbite.',
  },
  render: renderTemplate,
};
